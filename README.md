# Minikube

1.  Increase open Files, need for otel demo

        $ printf 'fs.nr_open = 1073741824\n' >> /etc/sysctl.d/local.conf
        $ cat >> /etc/security/limits.conf <<EOF
        # Minikube, give me more file descriptors!!
        *               -       nofile          -1
        root            -       nofile          -1
        EOF

2.  Increase pids, needed for otel demo

         $ cat >> /etc/containers/containers.conf <<EOF
         [containers]
         pids_limit=0
         EOF
         # check inside minikube container to confirm
         $ cat /sys/fs/cgroup/pids.max

3.  Start Minikube

        $ ./bootstrap
